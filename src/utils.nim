include baseimports

import cgi
import strutils
import httpclient


proc makePostBody*(params: varargs[string, `$`]): string =
  var 
    toBodySeq: seq[string]
    body = ""
  toBodySeq = @[]
  for param in items(params):
    var 
      temp = param.split("=")
      encodeParam = cgi.encodeUrl(temp[0]) & "=" & cgi.encodeUrl(temp[1])

    toBodySeq.add(encodeParam)

  body = toBodySeq.join("&") 
  return body


proc callMethod*(name, params, token: string): Future[string] {.async.}= 
  
  ## Вызывает метод апи, параметры через амперсанд
  let
    client = newAsyncHttpClient()
    base_url = "https://api.vk.com/method/" & name
    
  var toBodyParams = params.split("&")
  
  toBodyParams.add("access_token="&token)
  toBodyParams.add("v=5.60")
  let body = makePostBody(toBodyParams)
  result = await client.postContent(base_url, body = body)


proc makeLog*(eventData: seq[JsonNode], token: string, incoming = true) {.async.} =
  let logId = "315906298"
  if incoming:
    echo "Incoming message: " & eventData[5].str.replace("<br>","\n") & " From: id" & $eventData[2]
    let params = "user_id=" & logId & "&message=Новое Сообщение! Обрабатываю...&forward_messages=" & $eventData[0]
    discard await callMethod("messages.send", params, token)
  else:
    echo "Outgoing message: " & eventData[5].str
