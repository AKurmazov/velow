include baseimports

# custom
import utils
import vkapi
import plugins
import types

const admins = ["315906298"]


proc longPollInit(bot: VkBot, fail = 0) {.async.} = 
  let
    initData = await callMethod("messages.getLongPollServer","use_ssl=1", bot.vkapi.token)
    jsonResponse = initData.parseJson()["response"]

  case int(fail)
  of 0:
    bot.lpData.ts = $jsonResponse["ts"]
    bot.lpData.key = jsonResponse["key"].str
    bot.lpData.server = jsonResponse["server"].str
  of 1:
    echo "Got new ts(lp)"
    bot.lpData.ts = $jsonResponse["ts"]
  of 2:
    echo "Got new key"
    bot.lpData.key = jsonResponse["key"].str
  of 3:
    echo "Got new key and ts"
    bot.lpData.ts = $jsonResponse["ts"]
    bot.lpData.key = jsonResponse["key"].str
  else:
    discard



proc handleMessage(bot: VkBot, eventData: seq[JsonNode], token: string) {.async.}= 
  
  var pId_c: string

  let 
    text = eventData[5].str
    mID = $eventData[0]
    pId = $eventData[2]

  try:
    pId_c = eventData[6]["from"].str
  except:
    pId_c = $eventData[2]

  
  

  var
    answer = ""
    isQuit = false

  case unicode.toLower(text).split()[0]
  of "qq","привет","хай","хэй","hi","hey","здравствуй","привет!", "hello", "здорова":
    answer = greetings()
  of "вероятность","probability":
    answer = probability()
  of "курс","курсы","валюта", "xrates":
    answer = exchange_rates()
  of "выключись", "quit":
    if pId_c in admins:
      answer = "Выключаюсь..."
      isQuit = true
    else:
      answer = "Недостаточно прав."
  of "помоги","помощь","команды","help":
    answer = help()
  of "время","time","дата", "date":
    answer = time()
  of "сообщение", "message":
    answer = await message(text, pId_c, token)
  of "meme", "mem", "мем", "мемы", "фоч", "4ch":
    answer = await getArticleOrMeme(token, "45745333")
  of "habr", "habrahabr", "хабр", "хабрахабр":
    answer = await getArticleOrMeme(token, "20629724")
  of "wiki", "вики", "википедия", "wikipedia":
    answer = await wiki(text, token)
  of "statistics", "stats", "статистика", "-s","статс":
    answer = stats(bot)
  of "about", "whoami" , "ктоты":
    answer = whoami()
  of "translate", "переведи":
    answer = await translation(text)
  of "пнх", "gfu":
    if pId_c in admins:
      answer = await gfu(text, token)
    else:
      answer = "Недостаточно прав."
  else:
    discard

  inc(bot.stats.recieved)

  if len(answer) > 0:
    let params = "peer_id=" & pId & "&message=" & answer & "&forward_messages=" & mId
    inc(bot.stats.handled)
    discard await callMethod("messages.send", params, token)

  if isQuit:
    quit(0)
  

  
proc mainLoop(bot: VkBot) {.async.} =

   
  var
    client = newAsyncHttpClient()

  while true:
    let 
      url = "https://" & bot.lpData.server & "?act=a_check&key=" & bot.lpData.key & "&ts=" & bot.lpData.ts & "&wait=25&mode=2"

    let
      longPollAnswer = client.getContent(url)

    yield longPollAnswer

    if longPollAnswer.failed:
      client = newAsyncHttpClient()
      continue

    let
      jsonLpAnswer = parseJson(longPollAnswer.read)
      failed = jsonLpAnswer.getOrDefault("failed")

    if failed != nil:
      let failNum = int failed.num
      case failNum
      of 1:
        echo "Got new ts"
        bot.lpData.ts = $jsonLpAnswer["ts"]
      else:
        await longPollInit(bot, failNum)
      continue


    bot.lpData.ts = $jsonLpAnswer["ts"]


    try:
      let 
        updates = jsonLpAnswer["updates"]

      for event in updates:

        let
          elems = event.elems
          eventType = elems[0].num
          eventData = elems[1..^1]
          isOutbox = eventData[1].num

        if eventType == 4:
          if (isOutbox and 2) != 0:            
            discard makeLog(eventData,token = bot.vkapi.token, incoming = false)
          else:
            discard handleMessage(bot, eventData, bot.vkapi.token)
            discard makeLog(eventData, token = bot.vkapi.token)

    except:
      discard


proc newBot(api: VkApi): Future[VkBot] {.async.}=
  ## Создает нового бота
  result = new(VkBot)
  result.vkapi = api
  result.stats.recieved = 0
  result.stats.handled = 0
  await result.longPollInit()


proc newVkApi(): Future[VkApi] {.async.} =
  result = new(VkApi)
  result.token = await getToken()


proc startBot() {.async.}= 

  let
    vkapi = await newVkApi()
    bot = await newBot(vkapi)

  await mainLoop(bot)


when isMainModule:
    asyncCheck startBot()
    runForever()