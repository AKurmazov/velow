
include baseimports
import utils
import types


#Remove
import vkapi

const version = 1.0
const daddy = "https://vk.com/id315906298"
const greets = @["Привет!", "Здравствуй!", "Здравствуйте!"]
const commands = ["Приветствие - приветствует вас\n",
                  "Вероятность - предсказывает вероятность события\n",
                  "Курс - выводит актуальный курс валют\n",
                  "Выключись - выключает бота (требуются админ права)\n",
                  "Команды - выводит список доступных команд\n",
                  "Время - выводит текущее дату и время по MSK\n",
                  "Мем - выводит случайный мем из паблика 4ch\n",
                  "Сообщение - отправляет сообщение пользователю (anon/deanon)\n",
                  "Вики - выводит данные с википедии\n",
                  "Статистика - выводит статистику бота за эту сессию\n",
                  "Ктоты - выводит информацию о боте\n",
                  "Переведи - переводит введеный текст\n"]
                  

proc greetings*(): string = 
  result = random(greets)

proc whoami*(): string =
  result = "Меня зовут Велоу, я - бот, написанный на языке Nim.\nСоздатель: " & daddy

proc stats*(bot: VkBot): string =
  result = "За данную сессию:\n\nПринятно сообщений: $1\nОбработано команд: $2" % [$bot.stats.recieved, $bot.stats.handled]

proc probability*(): string =
  let probability = $random(100)
  result = "Вероятность составляет "&probability&"%"

proc exchange_rates*(): string = 
  let
    exApiUrl = "http://api.fixer.io/latest?base=RUB"
    client = newHttpClient()
    jsonResponse = client.getContent(exApiUrl).parseJson()
    USD = $round(1 / jsonResponse["rates"]["USD"].fnum, 2)
    EUR = $round(1 / jsonResponse["rates"]["EUR"].fnum, 2)

  result = "1 USD - " & USD & " RUB\n1 EUR - " & EUR & " RUB"
  

proc help*(): string = 
  result = "Список доступных комманд:\n\n"
  for c in commands:
    result.add(c)


proc time*(): string =
  let
    datetime =`$`(getGMTime(getTime() + 3.hours)).split("T")
    time = datetime[1].split("+")[0]
    date = datetime[0]
  
 
  result = "Формат: MSK(GMT+3)\nДата: $1\nВремя: $2".format(date, time)


proc message*(text: string, pId: string, token: string): Future[string] {.async.}= 
  let messageData = text.split()
  var
    isAnon: bool
    selfMessage: string

  if messageData.len < 4:
    return "Usage: message <reciever id> <anon/deanon> <text>"

  let
    recieverId = messageData[1]
    anon = messageData[2]
    messageTextSeq = messageData[3..^1]
    messageText = messageTextSeq.join(" ")


  case unicode.toLower(anon)
  of "anon","anonymously","анонимно","анон":
    isAnon = true
  of "deanon", "неанонимно","деанон":
    isAnon = false

  
  if isAnon:
    selfMessage = "У вас новое сообщение!\nОтправитель выбрал анонимный способ отправки.\n\nСообщение:\n" & messageText
  else:
    selfMessage = "У вас новое сообщение!\nОтправитель: https://vk.com/id" & pId & ".\n\nСообщение:\n" & messageText
    
  let 
    params = "user_id=" & recieverId & "&message=" & selfMessage
    response = await callMethod("messages.send", params, token)
    jsonResponse = response.parseJson

  if "error" in jsonResponse:
    result = "Неудалось отправить сообщение:\n" & jsonResponse["error"]["error_msg"].str
  else:
    result = "Сообщение успешно отправленно!"

  
proc getArticleOrMeme*(token: string, public_id: string): Future[string] {.async.} = 

  #45745333 - 4ch
  let
    params = "owner_id=-" & public_id
    response = await callMethod("wall.get", params, token)
    jsonResponse = response.parseJson
    memes = jsonResponse["response"]["items"]
    memeId = $random(toSeq(memes))["id"]
  
  result = "Держи!&attachment=wall-$1_$2" % [public_id, memeId]
  

proc wiki*(text: string, token: string): Future[string] {.async.} =
  var
    ans: string

  let
    search = unicode.toLower(join(text.split()[1..^1], " "))
    inpop = "url"
    action = "opensearch"
    format = "json"
    prop = "info"
    client = newAsyncHttpClient()
    postBody = makePostBody(@["search=" & search, "inpop=" & inpop, "action=" & action, "format=" & format, "prop=" & prop])
    postUrl = "https://ru.wikipedia.org/w/api.php"
    response = await client.postContent(postUrl, body = postBody)
    jsonResponse = response.parseJson


  if len(jsonResponse[3]) != 0:
    if unicode.toLower(jsonResponse[2][0].str) == search & ":" or unicode.toLower(jsonResponse[2][0].str).startsWith(search & " — многознач"):
      ans = jsonResponse[2][1].str
    else:
      ans = jsonResponse[2][0].str
    result = ans & "\n\nСписок найденных статей:\n"
    for i in jsonResponse[3]:
      result.add(i.str & "\n")
  else:
    result = "Ничего не найдено"
    
proc translation*(text: string): Future[string] {.async.} =
  let tempSeq = text.split()
  if tempSeq.len < 3 or len(tempSeq[1]) != 2:
    return "Usage: переведи <lang> <text>"

  let
    client = newAsyncHttpClient()
    textToTranslate = join(text.split()[2..^1], " ")
    lang = tempSeq[1]
    key = "trnsl.1.1.20170715T153555Z.859a6d17a582054c.ba8bb6a3a0f2634b7f824b1c550c9846b013e6dc"
    url = "https://translate.yandex.net/api/v1.5/tr.json/translate?"
    body = makePostBody("key=" & key, "lang=" & lang, "text=" & textToTranslate)
  
  try:
    let
      response = await client.getContent(url & body)
      jsonResponse = response.parseJson
    result = "Язык перевода: " & lang & "\nПереводимый текст: " & textToTranslate & "\nПереведенный текст: "
    result.add(jsonResponse["text"][0].str)
  except:
    return "Один из параметров некорректен"


proc gfu*(text: string, token: string): Future[string] {.async.} =
  let tempSeq = text.split()
  if len(tempSeq) != 2:
    return "Usage: пнх <id>"

  let
    gfuId = tempSeq[1]
    params = "user_id=" & gfuId & "&attachment=wall-143566817_97&message=Кто-то вас не любит, так что..."
  discard await callMethod("messages.send", params, token)

  result = "Готово!"

