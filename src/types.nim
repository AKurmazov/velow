include baseimports

type
  LpData* = object
    key*: string
    ts*: string
    server*: string

  VkApi* = ref object
    token*: string

  Stats* = object
    recieved*: int
    handled*: int

  VkBot* = ref object
    vkApi*: VkApi
    lpData*: LpData
    stats*: Stats 
