
include baseimports

#Свои модули
import utils
import config


#Параметра запроса для получения токена
const
  client_id = "client_id=3140623"
  client_secret = "client_secret=VeWdmVclDCtn6ihuP1nt"
  grant_type = "grant_type=password" 
  scope = "scope=all"
  v = "v=5.60"

let
  username = "username="& config.login 
  password = "password="& config.password 

proc getToken*(): Future[string] {.async.} =
  let 
    client = newAsyncHttpClient() 
    tokenRequest = "https://oauth.vk.com/token"
    body = utils.makePostBody(client_id, client_secret, grant_type, username, password, scope, v)
    jsonResponse =  await client.postContent(tokenRequest, body = body)
    token = jsonResponse.parseJson()["access_token"].str
  
  result = token